<!DOCTYPE html>
<html lang="en-US">
<head>
  <title>Kount</title>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <link rel="stylesheet" href="/templates/dist/styles/global.css"/>
  <link rel="stylesheet" href="/templates/dist/styles/main.css"/>
  <link rel="stylesheet" href="https://use.typekit.net/knt5yju.css">
<!--  <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">-->
  <link rel="stylesheet" href="https://use.typekit.net/cre0evq.css">
</head>
<body class="refresh19">
<header>
  <div class="top-nav desktop">
    <div class="container">
      <ul>
        <li>Sales 18009192167</li>
        <li><a href="#" class="link">Log In</a></li>
      </ul>
    </div>
  </div>
  <nav>
    <div class="container">
      <div class="logo">
        <a href="#">
          <img src="/templates/dist/images/svg/kount_logo.svg" alt="Kount logo">
        </a>
      </div>
      <div class="secondary-nav">
        <ul class="resp-search">
          <li class="search-wrap">
                <span class="search">
                     <img src="/templates/dist/images/search-icon.svg" alt="Search Icon">
                </span>
          </li>
        </ul>
        <div class="hamburger">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
      <div class="nav-wrap">
        <div class="main-nav">
          <ul>
            <li class="inner-nav" id="products">
              <a href="#" class="link">Products</a>
            </li>
            <li class="inner-nav" id="use_cases">
              <a href="#" class="link">Use Cases</a>
            </li>
            <li class="inner-nav" id="resources">
              <a href="#" class="link">Resources</a>
            </li>
            <li class="inner-nav" id="partners">
              <a href="" class="link">Partners</a>
            </li>
            <li class="inner-nav" id="about_us">
              <a href="#" class="link">About Us</a>
            </li>
            <div class="resp-top-nav">
              <ul>
                <li>Sales 18009192167</li>
                <li><a href="#" class="link">Log In</a></li>
              </ul>
            </div>
            <li class="btn-wrap">
              <a class="btn-default btn-contact" href="#" data-text="Contact Us"><span>Contact Us</span></a>
            </li>
            <li class="search-wrap">
                <span class="search">
                     <img src="/templates/dist/images/search-icon.svg" alt="Search Icon">
                </span>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
</header>