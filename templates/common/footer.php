<footer>
 <div class="container">
    <div class="content-wrapper">
       <div class="col first-block">
          <div class="logo-box">
            <a href="#"><img src="/templates/dist/images/svg/kount_logo.svg" alt="kount logo"></a>
          </div>
          <p>&copy;2019 Kount Inc. All Rights Reserved</p>
         <ul class="term-link">
           <li><a href="#">Terms of Use</a></li>
           <li><a href="#">Compliance</a></li>
           <li><a href="#">Privacy Policy</a></li>
           <li><a href="#">GDPR</a></li>
           <li> <a href="#">Site Map</a></li>
         </ul>
       </div>
       <div class="col second-block">
         <ul class="about-link">
           <li class="main-link">About</li>
           <li><a href="#">Board of Directors</a></li>
           <li><a href="#">Careers</a></li>
           <li><a href="#">Company Culture</a></li>
           <li><a href="#">Contact Us</a></li>
           <li><a href="#">Management Team</a></li>
         </ul>
       </div>
      <div class="col third-block">
        <ul class="resource-link">
          <li class="main-link">Resources</li>
          <li><a href="#">2 Minutes on Fraud</a></li>
          <li><a href="#">Glossary of Terms</a></li>
      </div>
      <div class="col fourth-block">
        <ul class="social-link">
          <li><a href="#"><img src="/templates/dist/images/svg/Twitter.svg" alt="twitter logo"></a></li>
          <li><a href="#"><img src="/templates/dist/images/svg/Facebook.svg" alt="fb logo"></a></li>
          <li><a href="#"><img src="/templates/dist/images/svg/Instagram.svg" alt="instagram logo"></a></li>
          <li><a href="#"><img src="/templates/dist/images/svg/linkedin.svg" alt="linkedin logo"></a></li>
          <li><a href="#"><img src="/templates/dist/images/svg/youtube.svg" alt="youtube logo"></a></li>
      </div>
    </div>
 </div>
</footer>
<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="/templates/dist/scripts/global.js"></script>
<script src="/templates/dist/scripts/redesign.js"></script>
</body>
</html>