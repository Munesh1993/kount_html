<section class="intro-with-box">
  <div class="pattern wow fadeInRight">
    <img src="/templates/dist/images/dot-pattern.png" alt="dot-pattern">
  </div>
  <div class="container">
    <div class="intro-block">
      <h2>Cras at venenatis sapien phasellus.</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a neque sed lectus hendrerit.</p>
    </div>
    <div class="column-wrapper">

      <div class="col-three text-blue">
<!--        <div class="hover-content">-->
          <div class="content">
            <div class="logo">
              <img src="/templates/dist/images/svg/payments.svg" alt="payments">
            </div>
            <h5>Payments Fraud Prevention</h5>
            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Curabitur sodales pulvinar eros</p>
            <a href="#" class="btn-default" data-text="Learn More"><span>learn more</span></a>
          </div>
<!--        </div>-->
      </div>

      <div class="col-three text-green">
<!--        <div class="hover-content">-->
          <div class="content">
            <div class="logo">
              <img src="/templates/dist/images/svg/new-account.svg" alt="new-account">
            </div>
            <h5>New Account
              Fraud Prevention</h5>
            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Curabitur sodales pulvinar eros
              Lorem ipsum dolor sit amet consectetur adipiscing elit. Curabitur sodales pulvinar eros</p>
            <a href="#" class="btn-default" data-text="Learn More"><span>learn more</span></a>
          </div>
<!--        </div>-->
      </div>

      <div class="col-three text-yellow">
<!--        <div class="hover-content">-->
          <div class="content">
            <div class="logo">
              <img src="/templates/dist/images/svg/account-takeover.svg" alt="account-takeover">
            </div>
            <h5>Account Takeover Protection</h5>
            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Curabitur sodales pulvinar eros</p>
            <a href="#" class="btn-default" data-text="Learn More"><span>learn more</span></a>
          </div>
<!--        </div>-->
      </div>

    </div>
  </div>
</section>