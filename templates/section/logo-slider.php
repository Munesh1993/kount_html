
<section class="logo-slider">
  <div class="container">
    <div class="slider-wrap">
      <div class="slider">

        <div class="item">
          <div class="logo">
            <img src="/templates/dist/images/earth.png" alt="earth">
          </div>
        </div>

        <div class="item">
          <div class="logo">
            <img src="/templates/dist/images/code.png" alt="code">
          </div>
        </div>

        <div class="item">
          <div class="logo">
            <img src="/templates/dist/images/treva.png" alt="treva">
          </div>
        </div>

        <div class="item">
          <div class="logo">
            <img src="/templates/dist/images/fox.png" alt="fox">
          </div>
        </div>

        <div class="item">
          <div class="logo">
            <img src="/templates/dist/images/zoo.png" alt="zoo">
          </div>
        </div>

        <div class="item">
          <div class="logo">
            <img src="/templates/dist/images/kyan.png" alt="kyan">
          </div>
        </div>

        <div class="item">
          <div class="logo">
            <img src="/templates/dist/images/earth.png" alt="earth">
          </div>
        </div>

        <div class="item">
          <div class="logo">
            <img src="/templates/dist/images/code.png" alt="code">
          </div>
        </div>

        <div class="item">
          <div class="logo">
            <img src="/templates/dist/images/treva.png" alt="treva">
          </div>
        </div>

        <div class="item">
          <div class="logo">
            <img src="/templates/dist/images/fox.png" alt="fox">
          </div>
        </div>

        <div class="item">
          <div class="logo">
            <img src="/templates/dist/images/zoo.png" alt="zoo">
          </div>
        </div>

        <div class="item">
          <div class="logo">
            <img src="/templates/dist/images/kyan.png" alt="kyan">
          </div>
        </div>

      </div>
      <div class="info">
        <p>This is a short line about how you are the trusted solution providers for the great companies above.</p>
        <a href="#" class="link-arrow">View all Case Stories</a>
      </div>
    </div>
  </div>
</section>