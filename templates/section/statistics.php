<section class="statistics">
  <div class="container">
    <div class="column-wrapper">
      <div class="col-two">
        <div class="col circle-one wow fadeInUp">
          <div class="progress ">
            <svg width="200" height="200">
              <circle class="outer" cx="90" cy="100" r="95" transform="rotate(-90, 95, 95)"></circle>
            </svg>
            <div class="v-middle-inner">
              <div class="v-middle">
                <div class="value"><span class="counter timer" data-to="40" data-speed="2000" data-count="40">0</span>m
                </div>
                <p>lorem</p>
              </div>
            </div>
          </div>
        </div>

        <div class="col circle-two wow fadeInUp">
          <div class="progress">
            <svg width="200" height="200">
              <circle class="outer" cx="90" cy="100" r="95" transform="rotate(-90, 95, 95)"></circle>
            </svg>
            <div class="v-middle-inner">
              <div class="v-middle">
                <div class="value"><span class="counter timer" data-to="25" data-speed="2000" data-count="67">0</span>%
                </div>
                <p>ipsum</p>
              </div>
            </div>
          </div>
        </div>

        <div class="col circle-three wow fadeInUp">
          <div class="progress">
            <svg width="200" height="200">
              <circle class="outer" cx="90" cy="100" r="95" transform="rotate(-90, 95, 95)"></circle>
            </svg>
            <div class="v-middle-inner">
              <div class="v-middle">
                <div class="value">$<span class="counter timer" data-to="3" data-speed="1200" data-count="3">0</span>m
                </div>
                <p>dolor</p>
              </div>
            </div>
          </div>
        </div>

        <div class="col circle-four wow fadeInUp">
          <div class="progress">
            <svg width="200" height="200">
              <circle class="outer" cx="90" cy="100" r="95" transform="rotate(-90, 95, 95)"></circle>
            </svg>
            <div class="v-middle-inner">
              <div class="v-middle">
                <div class="value"><span class="counter timer" data-to="26" data-speed="2000" data-count="26">0</span>
                </div>
                <p>sit</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-two">
        <div class="content">
        <h2>Lorem ipsum dolor.</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin est nisl, faucibus a eros vel, iaculis varius
          urna. Suspendisse eu sapien a ligula ultricies blandit. </p>
      </div>
      </div>

    </div>
  </div>
</section>