<section class="footer-cta">
  <div class="img-wrap">
          <div class="img-box">
            <img src="/templates/dist/images/svg/hexagon-white.svg" alt="white hexagon">
          </div>
          <div class="inner-img">
            <img src="/templates/dist/images/hexagon-cta.png" alt="people meeting">
          </div>
        </div>

  <div class="container">
       <div class="right">
          <h2>Proin gravida ipsum velit Doloremagna aliqua sit?</h2>
           <p>Ut enim ad minim veniam, quis nostrud excercitation ullamco laboris nisi ut aliquip ex ea</p>
        <div class="btn-wrap">
           <a href="#" class="btn-white btn-gap" data-text="Get a Demo"><span>Get a Demo</span></a>
          <a href="#" class="btn-white" data-text="Contact Us"><span>Contact Us</span></a>
        </div>
       </div>
   </div>

</section>