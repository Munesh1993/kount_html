<section class="hero-banner">
  <div class="bg-img">
    <img src="" alt="banner-second">
  </div>
  <div class="v-middle-wrapper">
    <div class="v-middle-inner container">
      <div class="v-middle">
        <div class="right wow fadeInUp">
           <h1>This is a headline about the problems you solve.</h1>
           <p>And a sub-line about who you are.</p>
           <a href="#" class="btn-white" data-text="Learn More"><span>Learn More</span></a>
        </div>
      </div>
    </div>
  </div>
  <div class="head-tab">
  <ul class="nav nav-tabs tab-links" id="myTab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="who-we" data-toggle="tab" href="#who-we" role="tab" aria-controls="who-we" aria-selected="true">Who We Are</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="releases" data-toggle="tab" href="#releases" role="tab" aria-controls="releases" aria-selected="false">New Release</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="events" data-toggle="tab" href="#events" role="tab" aria-controls="events" aria-selected="false">Upcoming Events</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="reports" data-toggle="tab" href="#reports" role="tab" aria-controls="reports" aria-selected="false">Latest Reports</a>
    </li>
  </ul>
<!--  <div class="tab-content" id="myTabContent">-->
<!--    <div class="tab-pane fade " id="who-we" role="tabpanel" aria-labelledby="who-we">Who we are</div>-->
<!--    <div class="tab-pane fade " id="releases" role="tabpanel" aria-labelledby="releases">Food truck</div>-->
<!--    <div class="tab-pane fade" id="events" role="tabpanel" aria-labelledby="events"> </div>-->
<!--    <div class="tab-pane fade" id="reports" role="tabpanel" aria-labelledby="reports"> </div>-->
<!--  </div>-->
  </div>
</section>

