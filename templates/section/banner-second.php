<section class="banner-second">
  <div class="bg-img">
    <img src="/templates/dist/images/tbd-banner.png" alt="banner-second">
  </div>
  <div class="v-middle-wrapper">
    <div class="v-middle-inner container">
      <div class="v-middle">
        <div class="content-outer wow fadeInUp">
          <h1>Lorem Ipsum dolor sit amet, consectetur adipiscing elit</h1>
          <p>Lorem Ipsum dolor sit amet, consectetur adipiscing elit Lorem Ipsum dolor sit amet, consectetur adipiscing
            elit Lorem Ipsum dolor sit amet, consectetur adipiscing elit</p>
        </div>
      </div>
    </div>
  </div>
</section>

