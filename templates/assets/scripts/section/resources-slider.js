// // Use $ instead of jQuery without replacing global $
(function ($) {
  $(window).on("load", function () {
    $('.resources-slider .slider-wrap').css({"opacity":1});
    $('.resources-slider .slider-wrap .slider .item').matchHeight();
    $('.resources-slider .slider-wrap .slider .item .content').matchHeight();
    $('.resources-slider .slider-wrap .slider .item .content h5').matchHeight();

    $('.resources-slider .slider-wrap .slider').slick({
      arrows: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      speed: 800,
      focusOnSelect: false,
      fade: false,
      infinite: true,
      draggable: false,
      dots: false,
      prevArrow: $('.resources-slider .slide-nav .slide-prev'),
      nextArrow: $('.resources-slider .slide-nav .slide-next'),
     // responsive: [
     //   {
     //     breakpoint: 992,
     //     settings: {
     //       infinite: true,
     //     }
     //   },
     //   {
     //     breakpoint: 768,
     //     settings: {
     //       slidesToShow: 1,
     //       slidesToScroll: 1,
     //       infinite: true,
     //     }
     //   }
     // ]
    });
  });

})(jQuery);