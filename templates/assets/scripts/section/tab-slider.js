// // Use $ instead of jQuery without replacing global $
(function ($) {

  $(document).ready(function () {

    $('.tab-slider .slider-for').slick({
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      infinite: true,
      speed: 800,
      draggable: false,
      asNavFor: '.slider-nav',
      // prevArrow: $('.tab-slider .slide-nav .slide-prev'),
      // nextArrow: $('.tab-slider .slide-nav .slide-next'),
    });

    $('.tab-slider .slider-nav').slick({
      arrows: false,
      slidesToShow: 6,
      slidesToScroll: 1,
      asNavFor: '.slider-for',
      infinite: true,
      draggable: false,
      speed: 800,
      dots: false,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 4
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 4
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 595,
          settings: {
            slidesToShow: 2,
            draggable: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            draggable: true
          }
        }
      ]
    });

  });

})(jQuery);