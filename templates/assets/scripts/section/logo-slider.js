// // Use $ instead of jQuery without replacing global $
(function ($) {
  $(window).on("load", function () {
    $('.logo-slider .slider-wrap').css({"opacity":1});

    $('.logo-slider .slider-wrap .slider').slick({
      arrows: true,
      slidesToShow: 6,
      slidesToScroll: 1,
      speed: 800,
      focusOnSelect: false,
      fade: false,
      infinite: true,
      draggable: false,
      dots: false,
     // responsive: [
     //   {
     //     breakpoint: 992,
     //     settings: {
     //       infinite: true,
     //     }
     //   },
     //   {
     //     breakpoint: 768,
     //     settings: {
     //       slidesToShow: 1,
     //       slidesToScroll: 1,
     //       infinite: true,
     //     }
     //   }
     // ]
    });
  });

})(jQuery);