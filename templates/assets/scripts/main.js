(function ($) {
  // On DOM ready

  $(function () {
    var wow = new WOW(
      {
        boxClass: 'wow', // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset: 0, // distance to the element when triggering the animation (default is 0)
        // mobile: true, // trigger animations on mobile devices (default is true)
        // live: true, // act on asynchronously loaded content (default is true)
        callback: function (box) {
          $(box).addClass('animate-complete');
        },
      }
    );
    wow.init();

    // hero-slider slider init
    wow_hero_slider = new WOW(
      {
        boxClass: 'wow-hero-slider'
      }
    )
    wow_hero_slider.init();

  });

  $(document).ready(function () {
    var $IE_image = $('img.ie-responsive');
    objectFitImages($IE_image);
  });

})(jQuery);