#!/usr/bin/env bash

rm -rf html

mkdir html

cp *.html html/

mkdir html/templates

cp -rf templates/assets html/templates/assets

cp -rf templates/dist html/templates/dist

echo "Html files are ready now. Commit and push"